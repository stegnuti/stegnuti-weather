﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegnutiWeather
{
    public class WeatherReading
    {
        public DateTime Time { get; }
        public double TempMin { get; }
        public double TempMax { get; }
        public double TempCurrent { get; }
        public double Pressure { get; }
        public double Humidity { get; }
        public double Visibility { get; }

        public double GetValue(Parameter param)
        {
            switch (param)
            {
                case Parameter.HUMIDITY:
                    return Humidity;
                case Parameter.VISIBILITY:
                    return Visibility;
                case Parameter.PRESSURE:
                    return Pressure;
                case Parameter.TEMPMIN:
                    return TempMin;
                case Parameter.TEMPCUR:
                    return TempCurrent;
                default:
                    return TempMax;
            }
        }

        public WeatherReading(API.DayWeather dayWeather)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            Time = dtDateTime.AddSeconds(dayWeather.dt).ToLocalTime();

            Visibility = dayWeather.visibility;
            Pressure = dayWeather.pressure;
            Humidity = dayWeather.humidity;

            // In Kelvins, need to conver to celsius
            TempMin = Math.Round(dayWeather.temp.min - 273.15, 1);
            TempMax = Math.Round(dayWeather.temp.max - 273.15, 1);
            TempCurrent = Math.Round(dayWeather.temp.day - 273.15, 1);
        }

        public WeatherReading(API.CurrentWeather currentWeather)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            Time = dtDateTime.AddSeconds(currentWeather.dt).ToLocalTime();

            Visibility = currentWeather.visibility;
            Pressure = currentWeather.pressure;
            Humidity = currentWeather.humidity;

            // In Kelvins, need to conver to celsius
            TempCurrent = Math.Round(currentWeather.temp - 273.15, 1);
        }

        public WeatherReading(API.HourWeather hourWeather)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            Time = dtDateTime.AddSeconds(hourWeather.dt).ToLocalTime();

            Visibility = hourWeather.visibility;
            Pressure = hourWeather.pressure;
            Humidity = hourWeather.humidity;

            // In Kelvins, need to conver to celsius
            TempCurrent = Math.Round(hourWeather.temp - 273.15, 1);
        }
    }
}
